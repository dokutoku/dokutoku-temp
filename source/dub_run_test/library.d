/**
 * library
 *
 * Examples:
 *--------------------
import std;
import dub_run_test.library;

void main()
{
	writeln(function_uint(10));
}
 *--------------------
 *
 * Author: $(LINK2 https://twitter.com/dokutoku3, dokutoku)
 * License: $(LINK2 https://creativecommons.org/publicdomain/zero/1.0/, CC0 1.0 Universal)
 */
module dub_run_test.library;


//version (EXTERN_C)
//version (EXTERN_D)
//version (EXTERN_CPLUS)
//version (EXTERN_WINDOWS)
//version (EXTERN_OBJECTIVEC)
//version (EXTERN_)
//version (PUBLIC)
//version (EXPORT)
//version (EXTERN)
//version (BINDING)
private static immutable return_types =
[
	"uint",
];

enum string funcs = () {
	string result;

	static foreach (type; return_types) {
		result ~= "/// " ~ type ~ "\n";

		version (EXTERN_C) {
			result ~= "extern (C)\n";
		} else version (EXTERN_CPLUS) {
			result ~= "extern (C++)\n";
		} else version (EXTERN_D) {
			result ~= "extern (D)\n";
		} else version (EXTERN_WINDOWS) {
			result ~= "extern (Windows)\n";
		}

		result ~= "pure nothrow @safe @nogc @live\n";

		version (PUBLIC) {
			result ~= "public ";
		} else version (EXPORT) {
			result ~= "export ";
		} else version (EXTERN) {
			result ~= "extern ";
		}

		result ~= type ~ " function_" ~ type ~  "(" ~ type ~  " input)";

		version (EXTERN) {
			result ~= ";";
		} else version (BINDING) {
			result ~= ";";
		} else {
			result ~= "{ return input; }";
		}
	}

	return result;
}();

mixin (funcs);
