/**
 * executable
 *
 * Author: $(LINK2 https://twitter.com/dokutoku3, dokutoku)
 * License: $(LINK2 https://creativecommons.org/publicdomain/zero/1.0/, CC0 1.0 Universal)
 */
module dub_run_test.executable;


version (EXECUTABLE_PROJECT):

version (EMPTY) {
} else {
	version = NOT_EMPTY;
}

version (WebAssembly) {
} else version (WASI) {
	version = WebAssembly;
} else {
	version = NOT_WASM;
}

version (D_BetterC) {
} else {
	version = NOT_BETTERC;
}

version (NOT_EMPTY) {
	version (D_BetterC) {
		import core.stdc.stdio;
	} else {
		import std.getopt;
		import std.stdio;
	}
}

version (WebAssembly)
version (EMPTY)
extern (C)
nothrow @nogc
void _start()

	do
	{
	}

version (NOT_WASM)
version (D_BetterC)
version (EMPTY)
pure nothrow @safe @nogc @live
extern (C)
int main(int argc, char** args)
{
	return 0;
}

version (NOT_WASM)
version (NOT_BETTERC)
version (EMPTY)
pure nothrow @safe @nogc @live
int main(string[] argv)
{
	return 0;
}

version (NOT_WASM)
version (D_BetterC)
version (NOT_EMPTY)
nothrow @nogc
int main(int argc, char** args)
{
	printf("Hello, World!");

	return 0;
}

version (NOT_WASM)
version (NOT_BETTERC)
version (NOT_EMPTY)
int main(string[] argv)
{
	string[] black_hole = null;

	auto help_information = std.getopt.getopt(
		argv,
		"black-hole", "Remove the specified arguments.", &black_hole,
	);

	if (help_information.helpWanted) {
		std.getopt.defaultGetoptPrinter("Some information about the program.", help_information.options);

		return 0;
	}

	for (size_t i = 0; i < argv.length; i++) {
		std.stdio.writefln!("argv[%d] = %s")(i, argv[i]);
	}

	return 0;
}
