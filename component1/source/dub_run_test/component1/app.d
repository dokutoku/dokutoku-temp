module dub_run_test.component1.app;


extern (C)
pure nothrow @safe @nogc @live
int lib()
{
	return 123;
}

extern (C++, "component1")
{
	pure nothrow @safe @nogc @live
	int int_value()
	{
		return 123;
	}
}
